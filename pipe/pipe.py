import os
import requests
from bitbucket_pipes_toolkit import Pipe, get_logger


PAGER_DUTY_SEND_ALERT_URL = 'https://events.pagerduty.com/v2/enqueue'

workspace = os.getenv('BITBUCKET_WORKSPACE', 'local')
repo = os.getenv('BITBUCKET_REPO_SLUG', 'local')
build = os.getenv('BITBUCKET_BUILD_NUMBER', 'local')

DEFAULT_INCIDENT_KEY = f'{workspace}/{repo}-event'
DEFAULT_PIPELINES_URL = f'https://bitbucket.org/{workspace}/{repo}/addon/pipelines/home#!/results/{build}'


schema = {
    "API_KEY": {
        "type": "string",
        "required": True
    },
    "INTEGRATION_KEY": {
        "type": "string",
        "required": True
    },
    "INCIDENT_KEY": {
        "type": "string",
        "default": DEFAULT_INCIDENT_KEY
    },
    "EVET_TYPE": {
        "type": "string",
        "default": 'trigger'
    },
    "SEVERITY": {
        "type": "string",
        "default": 'error'
    },
    "DESCRIPTION": {
        "type": "string",
        "default": 'Event triggered from Bitbucket Pipelines'
    },
    "CLIENT": {
        "type": "string",
        "default": 'Bitbucket Pipelines'
    },
    "CLIENT_URL": {
        "type": "string",
        "default": DEFAULT_PIPELINES_URL
    },
    "DEBUG": {
        "type": "boolean",
        "default": False
    },
}

logger = get_logger()


class PagerDutySendAlertPipe(Pipe):

    def run(self):
        super().run()

        api_key = self.get_variable('API_KEY')
        integration_key = self.get_variable('INTEGRATION_KEY')

        incident_key = self.get_variable('INCIDENT_KEY')
        event_type = self.get_variable('EVET_TYPE')
        severity = self.get_variable('SEVERITY')
        description = self.get_variable('DESCRIPTION')

        client = self.get_variable('CLIENT')
        client_url = self.get_variable('CLIENT_URL')

        header = {
            'Content-Type': 'application/json',
            'Authorization': f'Token token={api_key}'
        }

        payload = {
            "routing_key": integration_key,
            "event_action": event_type,
            "dedup_key": incident_key,
            "client": client,
            "client_url": client_url,
            "payload": {
                "summary": description,
                "source": DEFAULT_PIPELINES_URL,
                "severity": severity,
            }
        }

        logger.info('Sending and event to PageDuty...')

        response = requests.post(
            PAGER_DUTY_SEND_ALERT_URL,
            json=payload,
            headers=header)

        if response.status_code != 202:
            self.fail(message=f'Failed to send an event to pagerduty: {response.text}')

        self.success('Incident successfully created.')


if __name__ == '__main__':
    pipe = PagerDutySendAlertPipe(schema=schema)
    pipe.run()
