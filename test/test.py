import os

from bitbucket_pipes_toolkit.test import PipeTestCase


class PagerDutySendAlertTestCase(PipeTestCase):
    def test_no_parameters(self):
        result = self.run_container()
        self.assertRegex(result, '✖ Validation errors: \nAPI_KEY:\n- required field')

    def test_no_integration_key(self):
        result = self.run_container(
            environment={
                'API_KEY': 'foo',
            }
        )
        self.assertRegex(result, '✖ Validation errors: \nINTEGRATION_KEY:\n- required field')

    def test_success(self):
        result = self.run_container(
            environment={
                'API_KEY': os.getenv("API_KEY"),
                'INTEGRATION_KEY': os.getenv("INTEGRATION_KEY"),
                'DESCRIPTION': 'Test incident',
                'INCIDENT_KEY': '',
            }
        )

        self.assertRegex(result, '✔ Incident successfully created')
